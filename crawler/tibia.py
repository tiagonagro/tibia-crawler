import sys
import re
import dateparser
import requests
from bs4 import BeautifulSoup

from crawler.utils.checkers import check_char_type

from handlers.exceptions import APIException
from models.tibia import Character


class Crawler:
    def __init__(self, data):
        char = str(data.char)
        self.tipo: str = check_char_type(char)
        if self.tipo not in ["check"]:
            raise APIException(
                message="Nome não fornecido ou inválido.",
                status_code=400,
                error_name=APIException.REQUEST_ERROR,
            )
        self.char = re.sub(r"[\.|\-|\/]", "", char)

        self.response = dict(data=list(), message=None)

    def run(self):
        try:
            self._crawl()
        except APIException as e:
            raise e
        except Exception as e:
            raise e
        finally:
            pass

        return self.response

    def _crawl(self):
        btns_map = {
            "Submit": {"Submit": "Submit"}
        }

        session = requests.Session()

        # obtendo cookies da sessão
        session.head(
            "https://www.tibia.com/community/?subtopic=characters",
            verify=True,
        )

        url = "https://www.tibia.com/community/?subtopic=characters"
        payload = {
            "name": self.char if self.tipo == "check" else ""
            **btns_map[self.tipo],
        }
        resp = session.post(url, data=payload, allow_redirects=False, verify=False)
        if resp.status_code == 302:
            bs = BeautifulSoup(resp.text, "lxml")
            url_redirect = bs.find("a")["href"]
            url_redirect = url_redirect.replace("https", "http")
            resp = session.get(url_redirect, verify=False)

        resp.encoding = "UTF-8"  # corrigindo encoding da response

        if resp.status_code == 200:
            if re.search(r"nenhum registro encontrado", resp.text, re.I):
                self.response["message"] = "Nenhum registro encontrado."
            else:
                self.response["data"].append(self._parse(resp.text))

    def _parse(self, html) -> dict:
        try:
            tax_payer = Character()
            bs = BeautifulSoup(html, "lxml")

            trs = bs.find_all("tr")
            # removing the list's 1st element (strings redundancy)
            del trs[0]
            # removing leading and trailing spaces (strip)
            # filtering empty strings (if)
            trs = [s for tr in trs if (s := (tr.text.strip()))]
            # removing spaces excess
            trs = [re.sub(r"\s+", " ", tr) for tr in trs]

            text = "¿".join(trs)

            if m := re.search(r"name:\s*(.*?)¿", text, re.I):
                tax_payer.name = m.group(1)

            if m := re.search(r"title:\s*(.*?)¿", text, re.I):
                tax_payer.title = m.group(1)

            if m := re.search(r"sex:\s*(.*?)¿", text, re.I):
                tax_payer.sex = m.group(1)

            if m := re.search(r"vocation:\s*(.*?)¿", text, re.I):
                tax_payer.vocation = m.group(1)

            if m := re.search(r"level:\s*(.*?)¿", text, re.I):
                tax_payer.level = m.group(1)

            if m := re.search(r"achievement points:\s*(.*?)¿", text, re.I):
                tax_payer.achievement_points = m.group(1)

            if m := re.search(r"world:\s*(.*?)¿", text, re.I):
                tax_payer.world = m.group(1)

            if m := re.search(r"residence:\s*(.*?)¿", text, re.I):
                tax_payer.residence = m.group(1)

            if m := re.search(r"last login:\s*(.*?)¿", text, re.I):
                tax_payer.last_login = m.group(1)

            if m := re.search(r"comment:\s*(.*?)¿", text, re.I):
                tax_payer.comment = m.group(1)

            if m := re.search(r"account status:\s*(.*?)¿", text, re.I):
                tax_payer.account_status = m.group(1)

            return tax_payer.to_dict()
        except Exception as e:
            raise APIException(
                message=str(e),
                status_code=500,
                exception_type=type(e).__name__,
                error_name=APIException.PARSE_ERROR,
            )


if __name__ == "__main__":
    crawler = Crawler(sys.argv[1])
    print(crawler.run())
