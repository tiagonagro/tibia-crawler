import re


def check_char_type(doc: str) -> str:
    if re.fullmatch(r"^[A-Z a-z]*$", doc):
        return "check"

    return None
