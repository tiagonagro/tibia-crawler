import json
import logging
import traceback

import uvicorn
from dotenv import find_dotenv, load_dotenv
from fastapi import FastAPI, Request, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel

from crawler.tibia import Crawler as CrawlerBa
from handlers.exceptions import APIException
from models.tibia import RequestDataModel

load_dotenv(find_dotenv())
app = FastAPI()


def log_error(data: dict, base_url: str) -> None:
    logging.error(
        json.dumps(
            {
                "baseUrl": base_url,
                "requestBody": data,
                "traceback": traceback.format_exc(),
            }
        )
    )


class RequestModel(BaseModel):
    char: str


@app.post("/tibia", status_code=status.HTTP_200_OK)
async def post_tibia_characters(data: RequestModel, request: Request):
    try:
        data = RequestDataModel(**data.dict())
        crawler = CrawlerBa(data)
        return JSONResponse(content=crawler.run())
    except APIException as e:
        log_error(data.__dict__, request.url.path)
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"message": e.message, "error": e.error},
        )
    except Exception as e:
        log_error(data.__dict__, request.url.path)
        e = APIException(message=str(e), exception_type=type(e).__name__)
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content={"message": e.message, "error": e.error},
        )


if __name__ == "__main__":
    uvicorn.run(
        "app:app",
        host="0.0.0.0",
        port=8080,
        reload=True,
        debug=True,
        log_level="debug",
    )
