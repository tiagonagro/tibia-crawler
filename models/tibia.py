import datetime as dt
from typing import List

from handlers.exceptions import APIException


class Character(object):
    def __init__(self, **fields):
        self.name: str = fields.get("name", None)
        self.title: str = fields.get("title", None)
        self.sex: str = fields.get("sex", None)
        self.vocation: str = fields.get("vocation", None)
        self.level: str = fields.get("level", None)
        self.achievement_points: str = fields.get("achievement_points", None)
        self.world: str = fields.get("world", None)
        self.residence: str = fields.get("residence", None)
        self.last_login: dt.datetime = fields.get("last_login", None)
        self.comment: str = fields.get("comment", None)
        self.account_status: str = fields.get("account_status", None)
        self.expedition_date: dt.datetime = fields.get(
            "expedition_date",
            dt.datetime.now().replace(tzinfo=dt.timezone(dt.timedelta(hours=-3))),
        )

    def to_dict(self) -> dict:
        obj = self.__dict__
        for key in obj.keys():
            if isinstance(obj[key], dt.datetime):
                obj[key] = obj[key].isoformat()
        return obj


class RequestDataModel(object):
    def __init__(self, **fields):
        required_fields = ["char"]
        if not all(k in fields for k in required_fields):
            raise APIException(
                f"Missing request parameters ({required_fields})", status_code=400
            )

        self.char: str = fields.get("char", None)

