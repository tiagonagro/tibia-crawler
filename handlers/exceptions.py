class APIException(Exception):
    CRAWLER_ERROR = "CrawlerError"
    PARSE_ERROR = "ParseError"
    ELEMENT_ERROR = "ElementError"
    FILE_ERROR = "FileError"
    REQUEST_ERROR = "RequestError"
    CAPTCHA_ERROR = "CaptchaError"

    status_code = 500

    def __init__(
        self, message, status_code=None, exception_type=None, error_name=CRAWLER_ERROR,
    ):
        Exception.__init__(self)
        if not exception_type:
            exception_type = type(self).__name__
        self.message = f"{exception_type}: {message}"
        if status_code is not None:
            self.status_code = status_code
        self.error = error_name

    def to_dict(self):
        rv = self.__dict__
        rv["status_code"] = self.status_code
        return rv
