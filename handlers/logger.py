import logging
import json
import traceback

# from flask import request


def log_error(data: dict) -> None:
    logging.error(
        json.dumps(
            {
                # "baseUrl": request.base_url,
                "requestBody": data,
                "traceback": traceback.format_exc(),
            }
        )
    )
